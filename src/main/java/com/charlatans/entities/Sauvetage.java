package com.charlatans.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Sauvetage {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    private Integer sauveteurId;
    private Integer nombre_sauve;
    private String nom;
    private Date date;
    private String description;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSauveteurId() {
        return sauveteurId;
    }

    public void setSauveteurId(Integer sauveteurId) {
        this.sauveteurId = sauveteurId;
    }

    public Integer getNombre_sauve() {
        return nombre_sauve;
    }

    public void setNombre_sauve(Integer nombre_sauve) {
        this.nombre_sauve = nombre_sauve;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
