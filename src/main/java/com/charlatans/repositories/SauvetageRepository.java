package com.charlatans.repositories;

import com.charlatans.entities.Categorie;
import org.springframework.data.repository.CrudRepository;

import com.charlatans.entities.Sauvetage;

public interface SauvetageRepository extends CrudRepository<Sauvetage, Integer> {
}
