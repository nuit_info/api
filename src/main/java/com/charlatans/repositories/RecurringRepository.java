package com.charlatans.repositories;

import org.springframework.data.repository.CrudRepository;

import com.charlatans.entities.Recurring;

public interface RecurringRepository extends CrudRepository<Recurring, Integer> {
}
