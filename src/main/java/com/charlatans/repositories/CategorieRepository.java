package com.charlatans.repositories;

import org.springframework.data.repository.CrudRepository;

import com.charlatans.entities.Categorie;

public interface CategorieRepository extends CrudRepository<Categorie, Integer> {
}
