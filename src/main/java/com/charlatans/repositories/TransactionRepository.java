package com.charlatans.repositories;

import org.springframework.data.repository.CrudRepository;

import com.charlatans.entities.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

    public Iterable<Transaction> findByUserId(Integer userId);

}
