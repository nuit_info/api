package com.charlatans.repositories;

import org.springframework.data.repository.CrudRepository;

import com.charlatans.entities.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}
