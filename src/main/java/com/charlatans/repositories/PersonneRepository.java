package com.charlatans.repositories;

import com.charlatans.entities.Categorie;
import org.springframework.data.repository.CrudRepository;

import com.charlatans.entities.Personne;

public interface PersonneRepository extends CrudRepository<Personne, Integer> {
}
