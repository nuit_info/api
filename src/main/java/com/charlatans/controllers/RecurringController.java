package com.charlatans.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.charlatans.repositories.RecurringRepository;
import com.charlatans.entities.Recurring;

import java.util.Optional;

@Controller // This means that this class is a Controller
@RequestMapping(path="/api/recurring") // This means URL's start with /demo (after Application path)
public class RecurringController {
    @Autowired // This means to get the bean called recurringRepository
    // Which is auto-generated by Spring, we will use it to handle the data
    private RecurringRepository recurringRepository;

    @PostMapping(path="/add") // Map ONLY POST Requests
    public @ResponseBody String addNewRecurring (@RequestBody Recurring recurring) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request
        recurringRepository.save(recurring);
        return "Saved";
    }

    @PutMapping(path="/update") // Map ONLY POST Requests
    public @ResponseBody String updateRecurring (@RequestBody Recurring recurring) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request
        recurringRepository.findById(recurring.getId()).map(target -> {
            target.setIday(recurring.getIday());
            target.setPublishDate(recurring.getPublishDate());
            return target;
        });
        return "Updated";
    }

    @GetMapping(path="/{id}")
    public @ResponseBody Optional<Recurring> getRecurring(@PathVariable("id") Integer id) {
        return recurringRepository.findById(id);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Recurring> getAllRecurrings() {
        // This returns a JSON or XML with the recurrings
        return recurringRepository.findAll();
    }
}
