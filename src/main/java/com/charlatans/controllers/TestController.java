package com.charlatans.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // This means that this class is a Controller
public class TestController {

    @GetMapping(path="/api/") // Map ONLY POST Requests
    public @ResponseBody
    String addNewTransaction () {
        return "Hello";
    }

}
