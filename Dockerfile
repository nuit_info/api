FROM maven:3.8.4-jdk-11
COPY . /app/api
WORKDIR /app/api
RUN mvn clean package
ARG JAR_FILE=target/*.jar
RUN cp ${JAR_FILE} /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]